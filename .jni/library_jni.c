#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jobjectArray JNICALL Java_library_product_1
  (JNIEnv *env, jobject object, jobjectArray left, jint leftRows, jint leftColumns, jobjectArray right, jint rightRows, jint rightColumns, jobject productRows, jobject productColumns)
{
    javaEnv = env;
    int c_leftRows = toInt(leftRows);
    int c_leftColumns = toInt(leftColumns);
    int** c_left = toIntMatrix(left, c_leftRows, c_leftColumns);
    int c_rightRows = toInt(rightRows);
    int c_rightColumns = toInt(rightColumns);
    int** c_right = toIntMatrix(right, c_rightRows, c_rightColumns);
    int* c_productRows = intPtr(productRows);
    int* c_productColumns = intPtr(productColumns);
    int** c_outValue = product(c_left, c_leftRows, c_leftColumns, c_right, c_rightRows, c_rightColumns, c_productRows, c_productColumns);
    setJintMatrix(left , c_left, &c_leftRows, &c_leftColumns);
    setJintMatrix(right , c_right, &c_rightRows, &c_rightColumns);
    setIntegerValue(productRows, c_productRows);
    setIntegerValue(productColumns, c_productColumns);
    
    // Obtener la clase del objeto
    //jclass cls = (*env)->GetObjectClass(javaEnv, object);
    //jfieldID productRowsField = (*javaEnv)->GetFieldID(javaEnv, cls, "productRows", "Ljava/lang/Integer;");
    //(*javaEnv)->SetObjectField(javaEnv, object, productRowsField, c_productRows);
    //jfieldID productColumnsField = (*javaEnv)->GetFieldID(javaEnv, cls, "productColumns", "Ljava/lang/Integer;");
    //(*javaEnv)->SetObjectField(javaEnv, object, productColumnsField, c_productColumns);
    
    if (c_outValue == NULL) {
    	return NULL;
    }
    return toJintMatrix(c_outValue, c_productRows, c_productColumns);
}