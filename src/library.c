/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int** product(int** left, int leftRows, int leftColumns, int** right, int rightRows, int rightColumns, int* productRows, int* productColumns) {
    int** productResult = (int**) malloc(1 * sizeof(int*));
    productResult[0] = (int*) malloc(1 * sizeof(int));
    productResult[0][0] = -1000;
    *productRows = 1;
    *productColumns = 1;
    return productResult;
}
